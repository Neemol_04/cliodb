1. Authorities with more than one role 

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clio: <https://Cliodb.org#>
    #Authorities with more then one role

    SELECT DISTINCT ?name (COUNT(?roles) AS ?numRoles) WHERE
    {	   
       	?Authority a clio:Authority ;
                   clio:name ?name .
        ?Authority clio:hasRole ?roles .
    }
    GROUP BY(?name)
    HAVING(?numRoles > 5)

2. Return the number of events that museums held in ”Venice”. 

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    prefix clio: <https://Cliodb.org#> 
    SELECT (COUNT(?Event) AS ?numberOfEvents) WHERE
    {
       ?Event a clio:Event;
                clio:dateStart ?dateStart;
                clio:dateEnd ?dateEnd .
        ?Event clio:isLocated ?Location .
        ?Location a clio:Location .
        ?Location clio:province ?hasProvince .
        FILTER REGEX(?hasProvince, "[Vv]enezia") .
    }

3. Number of Archeological museum for every province

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clio: <https://Cliodb.org#>

    SELECT ?province (COUNT (?museum) AS ?numMuseums) WHERE
    {
	    ?museum a clio:MuseumInstitution .
        ?museum clio:discipline ?discipline .
        ?museum clio:isLocated ?loc .
        ?loc clio:province ?province .
        FILTER (?discipline = "Archeologia") .
    }
    GROUP BY (?province)
    ORDER BY DESC(?numMuseums)

4. Object in the province of Milan over the total object in the region

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clio: <https://Cliodb.org#>

    SELECT (?nobjMilano/?nobjLomb AS ?result) WHERE
    {	   
        {
            SELECT (COUNT(?obj) AS ?nobjMilano) WHERE{
			    ?obj a clio:CulturalObject .
        		?obj clio:isLocated ?loc .
            	?loc clio:province "Milano" .  
            }
        }
        {
            SELECT (COUNT(?obj) AS ?nobjLomb) WHERE {
                ?obj a clio:CulturalObject .
        		?obj clio:isLocated ?loc .
            	?loc clio:region "Lombardia" .
            }
        }
        }

5. The northernmost and southernmost museums and their difference in latitude

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clio: <https://Cliodb.org#>
     
    SELECT ?name1 ?city1 ?name2 ?city2 (xsd:float(?lat2) - xsd:float(?lat1) AS ?result)WHERE{
    {SELECT ?name1 ?lat1 ?city1 WHERE {
            ?museum a clio:MuseumInstitution .
        	?museum clio:name ?name1 .
            ?museum clio:isLocated ?loc .
            ?loc clio:latitude ?lat1 .
            ?loc clio:city ?city1 .
        FILTER(xsd:float(?lat1) > 30)
        }
            ORDER BY ASC(xsd:float(?lat1))
        LIMIT 1}
    {SELECT ?name2 ?lat2 ?city2 WHERE {
            ?museum a clio:MuseumInstitution .
        	?museum clio:name ?name2 .
            ?museum clio:isLocated ?loc .
            ?loc clio:latitude ?lat2 .
            ?loc clio:city ?city2 .
        FILTER(xsd:float(?lat2) > 30)
        }
    ORDER BY DESC(xsd:float(?lat2))
        LIMIT 1} 
    } 

6. Does Lombardy has more events than Lazio?

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clio: <https://Cliodb.org#>

    ASK WHERE{
        {
            SELECT (COUNT(?Event) AS ?evLazio)
        	WHERE {
        		?Event a clio:Event .
        		?Event clio:isLocated ?location .
        		?location clio:region ?region .
                FILTER REGEX(?region, "[Ll]azio") .
	    }
        }
        	{
            SELECT (COUNT(?Event) AS ?evLomb)
        	WHERE {
        		?Event a clio:Event .
        		?Event clio:isLocated ?location .
        		?location clio:region ?region .
                FILTER REGEX(?region, "[Ll]ombardia") .
		    }	
        }
        FILTER(?evLomb < ?evLazio)
    }

7. Return the names and city of the museums that are in addresses which contains “via Roma” in them.

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clio: <https://Cliodb.org#>
    SELECT DISTINCT ?name ?city WHERE
    {
        ?Museum a clio:MuseumInstitution .
        ?Museum clio:name ?name .
        ?Museum clio:isLocated ?Location .
        ?Location clio:city ?city .
        ?Location clio:address ?fullAddress .
        FILTER REGEX(?fullAddress, "via [Rr]oma.") .
    }

8. Number of cultural objects that are madonne con bambino

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX clio: <https://Cliodb.org#>
    SELECT (COUNT(?name) as ?numMadonne)
    WHERE {
        ?CO a clio:CulturalObject .
        ?CO clio:name ?name .
        FILTER REGEX(lcase(?name) , "madonna con bambino") .
    } 

9. Return all the websites of the museums that have “gov” as a top level domain.

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    prefix clio: <https://Cliodb.org#> 
    SELECT ?name ?website WHERE
    {
       ?Museum a clio:MuseumInstitution ;
       			clio:name ?name ;
       			clio:webSite ?website .
        FILTER REGEX(str(?website), "[[.]]gov") .
    }

10. Return the discipline, name, description, email, telephone, and website of the museums that their postcodes start with “6”.

    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    prefix clio: <https://Cliodb.org#> 

    SELECT ?discipline ?name ?description ?email ?telephone ?website ?postCode WHERE
    {
       ?Museum a clio:MuseumInstitution ;
       			clio:discipline ?discipline ;
       			clio:name ?name ;
       			clio:description ?description ;
          		clio:email ?email ;
       			clio:telephoneNumber ?telephone ;
       			clio:webSite ?website .
        ?Museum clio:isLocated ?Location .
        ?Location clio:postCode ?postCode .
        FILTER REGEX(xsd:string(?postCode), "^6") .
    }



