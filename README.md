# INSTRUCTIONS

## Prepare datasets
Clone repository

### Download the data from:
- https://dati.beniculturali.it/dataset/dataset-luoghi.ttl ;
- https://dati.beniculturali.it/dataset/dataset-eventiMeseCorrente.ttl ;
- https://dati.beniculturali.it/dataset/dataset-rlOA.ttl .

### Prepare the folder
1. Create in the root folder of the repository a new folder called "data_raw" ;
2. Put inside the "data_raw" folder the downloaded files .

## Extract the data
1. Run the pre_processing.ipynb notebook  ;
2. Run the data_extraction.pynb notebook (it takes a while for the last query) .

The output file will be found in the "data_raw" folder and will be called "output.ttl" with triples in turtle format. An example of the output.ttl file is present in the root folder. The computation can be checked in an unix-like system running in the terminal "diff output.ttl data_raw/output.ttl" (from the root folder).

## SPARQL

The queries are in the "SPARQL Queries" fodler inside a .txt file.

## OWL Ontology

The files that describe the ontology are in the "Ontology" folder in graphical form (PNG image and JSON file importable in arrows.app) and in OWL format. 

